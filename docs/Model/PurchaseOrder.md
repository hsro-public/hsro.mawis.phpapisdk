# # PurchaseOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **int** | ID objednávky |
**invoice** | **int** | Číslo faktury, kterou byl produkt zakoupen (_bewpi_invoice_number) | [optional]
**datebuy** | **\DateTime** | Datum objednávky |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
