# # User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID uživatele |
**lang_id** | **int** | Jazyk uživatele Požadované výchozí nastavení jazykové mutace aplikace. Z číselníku (tabulka wp_cis_language). |
**loginname** | **string** | Přihlašovací jméno |
**account_type** | **string** | * Typ účtu * P - soukromý účet #?vrací se vždy jenom P? * C - veřejný účet | [optional]
**validity_from** | **\DateTime** | Datum, od kdy je účet platný | [optional]
**validity_to** | **\DateTime** | Datum, do kdy je účet platný | [optional]
**firstname** | **string** | Jméno |
**surname** | **string** | Příjmení |
**titles** | **string** | Akademický &lt;strong&gt;titul&lt;/strong&gt; | [optional]
**phone_prv** | **string** | Telefon - Odesílá se vždy \&quot;null\&quot; | [optional]
**mobile_prv** | **string** | Mobil | [optional]
**email_prv** | **string** | E-mail | [optional]
**street_prv** | **string** | Korespondenční adresa - Ulice/část obce | [optional]
**number_prv** | **string** | Korespondenční adresa - Číslo domovní | [optional]
**city_prv** | **string** | Korespondenční adresa - Obec | [optional]
**zip_prv** | **string** | Korespondenční adresa - PSČ | [optional]
**country_id_prv** | **int** | Korespondenční adresa - Stát Odesílá se vždy \&quot;null\&quot;. Dříve se odesílal kód 203 dle jednoznačného idenitfikace státu. Nově se využívá atribut COUNTRY_CODE_PRV, kde se posílá CODE2 kompatiblní s Woocommerce pluginem WP. | [optional]
**company** | **string** | Základní údaje o subjektu - Název subjektu | [optional]
**function_org** | **string** | Funkce uživatele v organizaci - Odesílá se vždy \&quot;null\&quot; | [optional]
**entity_org** | **string** | Útvar v organizaci, do kterého je uživatel zařazen - Odesílá se vždy \&quot;null\&quot; | [optional]
**street_org** | **string** | Fakturační adresa - Ulice/část obce | [optional]
**number_org** | **string** | Fakturační adresa - Číslo domovní | [optional]
**city_org** | **string** | Fakturační adresa - Obec | [optional]
**zip_org** | **string** | Fakturační adresa - PSČ | [optional]
**country_id_org** | **int** | Fakturační adresa - Stát - Odesílá se vždy \&quot;null\&quot;. Dříve se odesílal kód 203 dle jednoznačného idenitfikace státu. Nově se využívá atribut COUNTRY_CODE_ORG, kde se posílá CODE2 kompatiblní s Woocommerce pluginem WP. | [optional]
**country_id** | **int** | Země registrace - Odesílá se jednoznačná identifikace státu z číselníku (tabulka wp_cis_country) nebo null pokud se nejedná o právnickou osobu a nemá tedy nastavenou zemi registrace. Identifikace základních údajů o subjektu pro právnickou osobu, dotahování dat z ARES pro 203/CZ[ČR]). | [optional]
**status** | **string** | Stav uživatelského účtu. &#39;A&#39; - aktivní, lze ho použít pro přihlášení &#39;N&#39; - neaktivní, nelze ho použít pro přihlášení **Odesílá se vždy &#39;A&#39;** |
**numberor_prv** | **string** | Korespondenční adreasa - Číslo orientační | [optional]
**numberor_org** | **string** | Fakturační adresa - Číslo orientační | [optional]
**in_code_id** | **int** | ID vstupního bodu - je povinně vyplněn pro všechny veřejné uživatele (obce/města, správce) resp. nikdy není vyplněn pro privátního (registrovaného) uživatele  Vstupní bod UR - Z číselníku (tabulka wp_in_code) | [optional]
**ico_org** | **string** | Základní údaje o subjektu - IČO | [optional]
**state_id** | [**\MawisApiClient\Model\SUBSTStateId**](SUBSTStateId.md) |  | [optional]
**info** | **string** | Informace, zda uživatel při registraci (nebo následně) povolil resp. nepovolil zasílání obchodních sdělení (A &#x3D; zasílání je povoleno, N &#x3D; není povoleno)| Odesílá se vždy \&quot;null\&quot; | [optional]
**datereg** | **\DateTime** | Datum registrace | [optional]
**autupd** | **int** | ID uživatele provedené změny | [optional]
**datupd** | **\DateTime** | Datum změny | [optional]
**gdpr_consent** | **string** | Souhlas s GDPR A - ano; N - ne |
**country_code_prv** | **string** | Korespondenční adresa - Stát | Odesílá se CODE2 | [optional]
**country_code_org** | **string** | Fakturační adresa - Stát | Odesílá se CODE2 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
