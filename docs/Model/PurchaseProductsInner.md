# # PurchaseProductsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID produktu | [optional]
**name** | **string** | Název produktu | [optional]
**typelicence** | **string** | Typ licence | [optional]
**role_id** | **int** | ID role (&#x3D; funkčnost), která je zakoupena v rámci produktu | [optional]
**number** | **int** | Počet kreditů | [optional]
**date_from** | **\DateTime** | Datum platnosti od - Datum objednávky |
**date_to** | **\DateTime** | Datum platnosti do - Datum objednávky + NUMMONTH |
**nummonth** | **int** | Časová platnost v měsících |
**numitems** | **int** | Množství |
**autupd** | **int** | ID uživatele provedené změny |
**datupd** | **\DateTime** | Datum změny |
**country_id** | **int** | Země produktu - Kód 203 dle jednoznačného idenitfikace státu (ČR) | [optional]
**codes** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
