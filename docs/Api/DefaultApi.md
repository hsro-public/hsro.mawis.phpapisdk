# MawisApiClient\DefaultApi

All URIs are relative to https://virtserver.swaggerhub.com/HSRO/WEBAPI/1.0, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**confirmPost()**](DefaultApi.md#confirmPost) | **POST** /confirm | Potvrzení registrace |
| [**purchasePost()**](DefaultApi.md#purchasePost) | **POST** /purchase | Odeslání objednávky |
| [**userIdPut()**](DefaultApi.md#userIdPut) | **PUT** /user/{id} | Editace uživatelem/Editace účtu správa vstupní bod |
| [**userPost()**](DefaultApi.md#userPost) | **POST** /user | Registrace uživatele |


## `confirmPost()`

```php
confirmPost($confirm)
```

Potvrzení registrace

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new MawisApiClient\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$confirm = new \MawisApiClient\Model\Confirm(); // \MawisApiClient\Model\Confirm

try {
    $apiInstance->confirmPost($confirm);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->confirmPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **confirm** | [**\MawisApiClient\Model\Confirm**](../Model/Confirm.md)|  | [optional] |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `purchasePost()`

```php
purchasePost($purchase)
```

Odeslání objednávky

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new MawisApiClient\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase = new \MawisApiClient\Model\Purchase(); // \MawisApiClient\Model\Purchase

try {
    $apiInstance->purchasePost($purchase);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->purchasePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **purchase** | [**\MawisApiClient\Model\Purchase**](../Model/Purchase.md)|  | [optional] |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `userIdPut()`

```php
userIdPut($id, $user)
```

Editace uživatelem/Editace účtu správa vstupní bod

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new MawisApiClient\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 34212; // int | id uživatele
$user = new \MawisApiClient\Model\User(); // \MawisApiClient\Model\User

try {
    $apiInstance->userIdPut($id, $user);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->userIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| id uživatele | |
| **user** | [**\MawisApiClient\Model\User**](../Model/User.md)|  | [optional] |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `userPost()`

```php
userPost($user)
```

Registrace uživatele

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new MawisApiClient\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user = new \MawisApiClient\Model\User(); // \MawisApiClient\Model\User

try {
    $apiInstance->userPost($user);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->userPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **user** | [**\MawisApiClient\Model\User**](../Model/User.md)|  | [optional] |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
